package com.charliechristensen.spacestation.spacestationpasses.data.cache

import com.charliechristensen.spacestation.spacestationpasses.data.models.PassInfo
import com.charliechristensen.spacestation.spacestationpasses.data.models.PassRequest
import com.charliechristensen.spacestation.spacestationpasses.data.models.StationPass
import junit.framework.Assert.fail
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * Tests the CacheImpl class
 */
class CacheTest {

    private val cachedCoordinate = 90.0
    private val nonCachedCoordinate = 80.0

    private val cache : Cache = CacheImpl()

    private val mockedPassInfo = Mockito.mock(PassInfo::class.java)
    private val mockedStationPass = StationPass("success", Mockito.mock(PassRequest::class.java), listOf(mockedPassInfo))

    @Before
    @Throws(Exception::class)
    fun setUp() {
        cache.setPassInfoForCoordinate(mockedStationPass, cachedCoordinate, cachedCoordinate)
    }

    @Test
    @Throws(Exception::class)
    fun getHasCacheForCoordinate() {
        cache.getHasCacheForCoordinate(cachedCoordinate, cachedCoordinate)
                .test()
                .assertValue { it }

        cache.getHasCacheForCoordinate(nonCachedCoordinate, nonCachedCoordinate)
                .test()
                .assertValue { !it }
    }

    @Test
    @Throws(Exception::class)
    fun getPassInfoForCoordinate() {
        cache.getPassInfoForCoordinate(cachedCoordinate, cachedCoordinate)
                .test()
                .assertValue { it == mockedStationPass }
        cache.getPassInfoForCoordinate(nonCachedCoordinate, nonCachedCoordinate)
                .test()
                .assertError(CacheMissException::class.java)
    }

    @Test
    @Throws(Exception::class)
    fun setPassInfoForCoordinate() {
        cache.setPassInfoForCoordinate(mockedStationPass, cachedCoordinate, cachedCoordinate)!! //Asserts not null
        try {
            cache.setPassInfoForCoordinate(null, cachedCoordinate, cachedCoordinate)
            fail("Did not throw exception when expected")
        }catch (exception : Exception){ }
    }

}