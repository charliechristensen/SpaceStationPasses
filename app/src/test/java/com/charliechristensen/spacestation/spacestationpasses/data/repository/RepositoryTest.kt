package com.charliechristensen.spacestation.spacestationpasses.data.repository

import com.charliechristensen.spacestation.spacestationpasses.data.SpaceStationService
import com.charliechristensen.spacestation.spacestationpasses.data.cache.Cache
import com.charliechristensen.spacestation.spacestationpasses.data.models.PassInfo
import com.charliechristensen.spacestation.spacestationpasses.data.models.PassRequest
import com.charliechristensen.spacestation.spacestationpasses.data.models.StationPass
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

/**
 * Repository test
 */
class RepositoryTest {

    private val networkCoordinate = 70.0
    private val cachedCoordinate = 90.0

    private val mockedPassInfo = mock(PassInfo::class.java)
    private val mockedStationPass = StationPass("success", mock(PassRequest::class.java), listOf(mockedPassInfo))

    private val cache: Cache = mock {
        on { getHasCacheForCoordinate(cachedCoordinate, cachedCoordinate) } doReturn Single.just(true)
        on { getHasCacheForCoordinate(networkCoordinate, networkCoordinate) } doReturn Single.just(false)
        on { getPassInfoForCoordinate(cachedCoordinate, cachedCoordinate)} doReturn Single.just(mockedStationPass)
    }

    private val service: SpaceStationService = mock {
        on { getSpaceStationPasses(networkCoordinate, networkCoordinate, 1)} doReturn Single.just(mockedStationPass)
    }

    private val repository = RepositoryImpl(cache, service)

    @Test
    @Throws(Exception::class)
    fun getPassTimesForCoordinates() {
        //Verified that the method completes and returns the proper data
        repository.getPassTimesForCoordinates(cachedCoordinate, cachedCoordinate, 1)
                .test()
                .assertValue { it.size == 1 && it[0] == mockedPassInfo}
        repository.getPassTimesForCoordinates(networkCoordinate, networkCoordinate, 1)
                .test()
                .assertValue { it.size == 1 && it[0] == mockedPassInfo}

        //Verify cached methods are invoked for cached data and network methods are
        //invoked for data that doesn't exist in cache
        verify(cache).getPassInfoForCoordinate(cachedCoordinate, cachedCoordinate)
        verify(service).getSpaceStationPasses(networkCoordinate, networkCoordinate, 1)


        verify(cache, never()).getPassInfoForCoordinate(networkCoordinate, networkCoordinate)
        verify(service, never()).getSpaceStationPasses(cachedCoordinate, cachedCoordinate, 1)

        //Verify cache update is called
        verify(cache).setPassInfoForCoordinate(mockedStationPass, cachedCoordinate, cachedCoordinate)
        verify(cache).setPassInfoForCoordinate(mockedStationPass, networkCoordinate, networkCoordinate)
    }
}