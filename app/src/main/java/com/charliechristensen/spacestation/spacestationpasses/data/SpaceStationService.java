package com.charliechristensen.spacestation.spacestationpasses.data;

import com.charliechristensen.spacestation.spacestationpasses.data.models.StationPass;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Retrofit interface for retrieving Space Station Passes from the network
 */

public interface SpaceStationService {

    @GET("iss-pass.json")
    Single<StationPass> getSpaceStationPasses(@Query("lat") double latitude, @Query("lon") double longitude, @Query("n") int count);

}
