package com.charliechristensen.spacestation.spacestationpasses.ui.stationPassList;


import com.charliechristensen.spacestation.spacestationpasses.ui.MvpPresenter;

/**
 * Presenter interface for the station pass list
 */

public interface StationPassListPresenter extends MvpPresenter<StationPassListView> {

    void refreshList();

    void locationUpdated(double latitude, double longitude);

    void noProvidersEnabledError();

    void noPermissionsError();

}
