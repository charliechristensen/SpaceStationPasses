package com.charliechristensen.spacestation.spacestationpasses.data.models

/**
 * A container to hold Coordinates as keys in the cache.
 * Data class provides default hashCode() and equals() methods.
 *
 * NOTE:
 * In a production app, we could provide a tolerance and override equals().
 * This way we could prevent multiple api requests when the coordinates are
 * within the tolerance
 *
 */
data class CacheCoordinate(val latitude: Double, val longitude: Double)