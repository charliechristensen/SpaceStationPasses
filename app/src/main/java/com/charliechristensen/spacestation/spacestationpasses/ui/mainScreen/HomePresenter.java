package com.charliechristensen.spacestation.spacestationpasses.ui.mainScreen;

import com.charliechristensen.spacestation.spacestationpasses.ui.MvpPresenter;

/**
 * Presenter interface for the home screen
 */

public interface HomePresenter extends MvpPresenter<HomeView> {
    void getStationPassesWithCoordinatesButtonClick(String latitudeString, String longitudeString);
    void getStationPassesWithCurrentLocationButtonClick();

    void locationPermissionsGranted();
    void locationPermissionDenied();
}
