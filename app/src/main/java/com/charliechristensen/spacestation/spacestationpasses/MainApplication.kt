package com.charliechristensen.spacestation.spacestationpasses

import android.app.Application
import com.charliechristensen.spacestation.spacestationpasses.dagger.BaseDependencyComponent
import com.charliechristensen.spacestation.spacestationpasses.dagger.BaseDependencyModule
import com.charliechristensen.spacestation.spacestationpasses.dagger.DaggerBaseDependencyComponent
import com.charliechristensen.spacestation.spacestationpasses.data.repository.Repository
import com.charliechristensen.spacestation.spacestationpasses.util.scheduler.RxScheduler
import javax.inject.Inject

/**
 * Main Application class where some singletons are injected and can be provided to Activities
 */

class MainApplication : Application(){

    @Inject lateinit var repository: Repository
    @Inject lateinit var scheduler: RxScheduler

    private val graph : BaseDependencyComponent by lazy {
        DaggerBaseDependencyComponent
                .builder()
                .baseDependencyModule(BaseDependencyModule("http://api.open-notify.org"))
                .build()
    }


    override fun onCreate() {
        super.onCreate()
        graph.inject(this)
    }

}
