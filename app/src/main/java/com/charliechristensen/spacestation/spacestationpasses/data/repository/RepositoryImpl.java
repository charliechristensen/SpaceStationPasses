package com.charliechristensen.spacestation.spacestationpasses.data.repository;

import com.charliechristensen.spacestation.spacestationpasses.data.SpaceStationService;
import com.charliechristensen.spacestation.spacestationpasses.data.cache.Cache;
import com.charliechristensen.spacestation.spacestationpasses.data.models.PassInfo;
import com.charliechristensen.spacestation.spacestationpasses.data.models.StationPass;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import io.reactivex.Single;

/**
 * Production implementation for repository
 */

public class RepositoryImpl implements Repository {
    private Cache cache;
    private SpaceStationService service;

    public RepositoryImpl(Cache cache,
                          SpaceStationService service){
        super();
        this.cache = cache;
        this.service = service;
    }

    @NotNull
    @Override
    public Single<List<PassInfo>> getPassTimesForCoordinates(double latitude, double longitude, int count) {
        return cache.getHasCacheForCoordinate(latitude, longitude)
                .flatMap ( hasCachedStationPass -> {
                    if (hasCachedStationPass) {
                        return cache.getPassInfoForCoordinate(latitude, longitude);
                    } else {
                        return service.getSpaceStationPasses(latitude, longitude, count);
                    }
                })
                .doOnSuccess (stationPass -> cache.setPassInfoForCoordinate(stationPass, latitude, longitude) ) // Update the cache
                .map(StationPass::getPassData);
    }
}
