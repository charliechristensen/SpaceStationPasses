package com.charliechristensen.spacestation.spacestationpasses.data.cache

/**
 * Created by Chuff on 12/18/17.
 */
class CacheMissException : Throwable("Object does not exist in cache")