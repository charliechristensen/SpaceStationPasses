package com.charliechristensen.spacestation.spacestationpasses.data.models

import java.util.*

/**
 * Data model which contains the duration and time the station passes over.
 */
data class PassInfo(private val duration: Int, private val risetime: Long) {

    /**
     * Returns a formatted version of the rise time
     *
     * This could be altered to return different formats
     *
     * NOTE: risetime is a Unix timestamp which is in seconds. The Date class takes
     * milliseconds so it must be multiplied by 1000
     *
     */
    fun getFormattedRiseTime(): String =
            Date(risetime * 1000L).toString()


    /**
     * Returns a formatted version of the duration
     */
    fun getFormattedDuration(): String =
            "$duration seconds"

}