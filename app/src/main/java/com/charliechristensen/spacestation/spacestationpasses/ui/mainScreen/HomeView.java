package com.charliechristensen.spacestation.spacestationpasses.ui.mainScreen;

import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * View interface for the Home Screen
 */

public interface HomeView extends MvpView {

    void showLatitudeError();
    void showLongitudeError();
    void hideLatitudeError();
    void hideLongitudeError();

    boolean getHasLocationPermissions();
    void requestLocationPermissions();

    void pushStationPassListActivity(double latitude, double longitude);
    void pushStationPassListActivityWithLocationMonitoring();

    void showPermissionDeniedError();
}
