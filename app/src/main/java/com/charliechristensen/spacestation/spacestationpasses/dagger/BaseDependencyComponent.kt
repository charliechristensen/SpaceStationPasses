package com.charliechristensen.spacestation.spacestationpasses.dagger

import com.charliechristensen.spacestation.spacestationpasses.MainApplication
import dagger.Component
import javax.inject.Singleton

/**
 * Dagger component for Base Dependencies intended to inject into MainApplication
 * I inject the repository into the main application so the cache stays alive for
 * the lifetime of the app.
 */
@Singleton
@Component(modules = [(BaseDependencyModule::class)])
interface BaseDependencyComponent {
    fun inject(application: MainApplication)
}