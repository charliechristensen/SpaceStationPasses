package com.charliechristensen.spacestation.spacestationpasses.dagger

import com.charliechristensen.spacestation.spacestationpasses.data.SpaceStationService
import com.charliechristensen.spacestation.spacestationpasses.data.cache.Cache
import com.charliechristensen.spacestation.spacestationpasses.data.cache.CacheImpl
import com.charliechristensen.spacestation.spacestationpasses.data.repository.Repository
import com.charliechristensen.spacestation.spacestationpasses.data.repository.RepositoryImpl
import com.charliechristensen.spacestation.spacestationpasses.util.scheduler.RxScheduler
import com.charliechristensen.spacestation.spacestationpasses.util.scheduler.RxSchedulerImpl
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Dagger module with some singletons
 */

@Module
class BaseDependencyModule(private val baseUrl: String) {

    @Provides
    @Singleton
    internal fun provideRepository(cache: Cache, apiService: SpaceStationService): Repository = RepositoryImpl(cache, apiService)

    @Provides
    @Singleton
    internal fun provideScheduler(): RxScheduler = RxSchedulerImpl()

    @Provides
    @Singleton
    internal fun provideCache(): Cache = CacheImpl()

    @Provides
    @Singleton
    internal fun provideRetrofit(): Retrofit =
            Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

    @Provides
    @Singleton
    internal fun provideApiService(retrofit: Retrofit): SpaceStationService =
            retrofit.create(SpaceStationService::class.java)

}
