package com.charliechristensen.spacestation.spacestationpasses.util.scheduler;

import io.reactivex.Scheduler;

/**
 * Interface for Schedulers
 *
 * Different implementations of this class give
 * us control over threading in different environments.
 *
 */

public interface RxScheduler {
    Scheduler mainThread();
    Scheduler backgroundThread();
}
