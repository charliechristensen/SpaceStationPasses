package com.charliechristensen.spacestation.spacestationpasses.ui.stationPassList;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.charliechristensen.spacestation.spacestationpasses.R;
import com.charliechristensen.spacestation.spacestationpasses.data.models.PassInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * RecyclerView Adapter to display lists of PassInfo
 */

public class StationPassListAdapter extends RecyclerView.Adapter<StationPassListAdapter.ViewHolder> {

    private final List<PassInfo> objects = new ArrayList<>();

    void addItems(List<PassInfo> items){
        objects.addAll(items);
        notifyDataSetChanged();
    }

    void clearItems(){
        objects.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.view_station_pass_detail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PassInfo passInfo = objects.get(position);
        holder.dateTextView.setText(passInfo.getFormattedRiseTime());
        holder.durationTextView.setText(passInfo.getFormattedDuration());
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.dateTextView)
        TextView dateTextView;
        @BindView(R.id.durationTextView)
        TextView durationTextView;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
