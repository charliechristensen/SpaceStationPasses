package com.charliechristensen.spacestation.spacestationpasses.ui.stationPassList

import com.charliechristensen.spacestation.spacestationpasses.data.models.PassInfo
import com.charliechristensen.spacestation.spacestationpasses.data.repository.Repository
import com.charliechristensen.spacestation.spacestationpasses.ui.BaseMvpPresenter
import com.charliechristensen.spacestation.spacestationpasses.util.CoordinateUtil
import com.charliechristensen.spacestation.spacestationpasses.util.scheduler.RxScheduler
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import java.io.IOException

/**
 * Presenter implementation for displaying the list of station passes
 */

class StationPassListPresenterImpl @JvmOverloads constructor(private val repository: Repository, private val scheduler: RxScheduler, private val shouldMonitorLocation: Boolean, private var latitude: Double = -181.0, private var longitude: Double = -181.0) : BaseMvpPresenter<StationPassListView>(), StationPassListPresenter {

    private val objectCount = 25 //How many objects to retrieve from network
    val objects: ArrayList<PassInfo> = ArrayList()//public for testing(for some reason it wont recognize internal even though in same package)

    //region Lifecycle

    /**
     * Once the views are bound we check to see if we should be monitoring our location.
     * Then we check to see if we have already loaded objects(could happen after a rotation etc.)
     */
    override fun onViewBound(view : StationPassListView) {
        super.onViewBound(view)
        if (shouldMonitorLocation) {
            view.startMonitoringLocation()
        }
        if (objects.isEmpty()) {
            if (!shouldMonitorLocation) {
                fetchStationPasses(latitude, longitude, true)
            }
        } else {
            addObjects(objects.toList(), true)
        }
    }

    override fun detachView() {
        if (shouldMonitorLocation) {
            ifViewAttached { view -> view.stopMonitoringLocation() }
        }
        super.detachView()
    }

    //endregion

    //region List Management

    fun fetchStationPasses(latitude: Double, longitude: Double, refresh: Boolean) {
        if (CoordinateUtil.areValidCoordinates(latitude, longitude)) {
            ifViewAttached { view -> view.setRefreshing(true) }
            repository.getPassTimesForCoordinates(latitude, longitude, objectCount)
                    .subscribeOn(scheduler.backgroundThread())
                    .observeOn(scheduler.mainThread())
                    .subscribeBy(
                            onSuccess = {
                                addObjects(it, refresh)
                            },
                            onError = {
                                handleError(it) }
                    )
                    .addTo(disposables)
        }else{
            ifViewAttached { view ->
                view.setRefreshing(false)
                view.setNoResultsTextViewVisible(objects.isEmpty())
                view.showInvalidCoordinatesError()
            }
        }
    }

    private fun addObjects(newObjects: List<PassInfo>, refresh: Boolean) {
        if (refresh) {
            objects.clear()
        }
        objects.addAll(newObjects)
        ifViewAttached { view ->
            view.addItems(objects, refresh)
            view.setNoResultsTextViewVisible(objects.isEmpty())
            view.setRefreshing(false)
        }
    }

    override fun refreshList() {
        if(shouldMonitorLocation) {
            ifViewAttached { view ->
                view.stopMonitoringLocation()
                view.startMonitoringLocation()
            }
        }else{
            fetchStationPasses(latitude, longitude, true)
        }
    }

    //endregion

    //region Location Updates

    override fun locationUpdated(latitude: Double, longitude: Double) {
        this.latitude = latitude
        this.longitude = longitude
        fetchStationPasses(this.latitude, this.longitude, true)
    }

    //endregion

    //region Error Reporting

    private fun handleError(throwable: Throwable) {
        ifViewAttached { view ->
            view.setNoResultsTextViewVisible(objects.isEmpty())
            view.setRefreshing(false)
            when (throwable) {
                is IOException -> {
                    view.showNetworkError()
                }
                else -> {
                    view.showUnknownError()
                }
            }
        }
    }

    override fun noProvidersEnabledError() {
        ifViewAttached { view ->
            view.setNoResultsTextViewVisible(objects.isEmpty())
            view.setRefreshing(false)
            view.showNoProvidersError()
        }
    }

    override fun noPermissionsError() {
        ifViewAttached { view ->
            view.setNoResultsTextViewVisible(objects.isEmpty())
            view.setRefreshing(false)
            view.showNoPermissionsError()
        }
    }

    //endregion

}
